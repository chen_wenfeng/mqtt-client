﻿using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Receiving;
using MQTTnet.Client.Subscribing;
using MQTTnet.Formatter;
using MQTTnet.Protocol;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinMqttClient
{
    public partial class FrmMqttClientMain : Form, IMqttClientConnectedHandler, IMqttApplicationMessageReceivedHandler, IMqttClientDisconnectedHandler
    {
        private IMqttClient mqttClient = null;
        private bool isCancelSend = false;
        private MqttFactory factory = new MqttFactory();

        public FrmMqttClientMain()
        {
            InitializeComponent();

            if (string.IsNullOrWhiteSpace(txtClientId.Text))
            {
                txtClientId.Text = Guid.NewGuid().ToString();
            }

        }

        //跨线程读取控件ComboBox的值，并返回
        public delegate string commbdelegate(ComboBox cb);
        public string commb(ComboBox cb)
        {
            if (cb.InvokeRequired)
            {
                commbdelegate dt = new commbdelegate(commb);
                IAsyncResult ia = cb.BeginInvoke(dt, new object[] { cb });
                return (string)cb.EndInvoke(ia);  //这里需要利用EndInvoke来获取返回值
            }
            else
            {
                return cb.Text;
            }
        }

        private IMqttClientOptions BuildOptions()
        {
            try
            {

                Invoke((new Action(() =>
                {
                    txtReceiveMessage.AppendText($">> Host={txtServerIP.Text}. ClientId={txtClientId.Text}{Environment.NewLine}");

                })));

                string server = txtServerIP.Text;
                int? port = null;
                if (!string.IsNullOrWhiteSpace(txtPort.Text))
                {
                    port = Convert.ToInt32(txtPort.Text);
                }
                string clientId = string.IsNullOrWhiteSpace(txtClientId.Text) == true ? Guid.NewGuid().ToString() : txtClientId.Text;
                string username = txtUsername.Text;


                var optionsBuilder = new MqttClientOptionsBuilder()
                    .WithTcpServer(server, port)
                    .WithClientId(clientId);
                if (!string.IsNullOrWhiteSpace(username))
                {
                    optionsBuilder.WithCredentials(username, Encoding.UTF8.GetBytes(txtPassword.Text));
                }
                if (chkCleanSession.Checked)
                {
                    optionsBuilder.WithCleanSession();
                }

                int keepAlivePeriod = string.IsNullOrWhiteSpace(txtKeepAlivePeriod.Text) ? 60 : Convert.ToInt32(txtKeepAlivePeriod.Text);
                int communicationTimeout = string.IsNullOrWhiteSpace(txtCommunicationTimeout.Text) ? 10 : Convert.ToInt32(txtCommunicationTimeout.Text);
                optionsBuilder.WithKeepAlivePeriod(TimeSpan.FromSeconds(keepAlivePeriod));
                optionsBuilder.WithCommunicationTimeout(TimeSpan.FromSeconds(communicationTimeout));
                string ver = commb(cbbMqttProtocolVersion);
                switch (ver)
                {
                    case "3.1.0":
                        optionsBuilder.WithProtocolVersion(MqttProtocolVersion.V310);
                        break;

                    case "5.0.0":
                        optionsBuilder.WithProtocolVersion(MqttProtocolVersion.V500);
                        break;

                    default:
                        optionsBuilder.WithProtocolVersion(MqttProtocolVersion.V311);
                        break;
                }

                IMqttClientOptions options = optionsBuilder.Build();
                return options;
            }
            catch (Exception ex)
            {
                Invoke((new Action(() =>
                {
                    txtReceiveMessage.AppendText($">> 读取参数错误{Environment.NewLine}");

                })));
                throw ex;
            }
        }

        private async Task ConnectMqttServerAsync()
        {
            if (mqttClient == null)
            {
                mqttClient = factory.CreateMqttClient();
                //mqttClient.ApplicationMessageReceived += MqttClient_ApplicationMessageReceived;
                //mqttClient.Connected += MqttClient_Connected;
                //mqttClient.Disconnected += MqttClient_Disconnected;
                mqttClient.ConnectedHandler = this;
                mqttClient.ApplicationMessageReceivedHandler = this;
                mqttClient.DisconnectedHandler = this;
            }

            try
            {
                IMqttClientOptions options = BuildOptions();

                /*               var options = new MqttClientOptions
                               {
                                   ClientId = Guid.NewGuid().ToString(),
                                   CleanSession = true,
                                   //UserName = "u001",
                                   //Password = "p001",
                                   ChannelOptions = new MqttClientTcpOptions
                                   {
                                       //Server = "localhost",
                                       Server = txtServerIP.Text,//"192.168.1.196",// "192.168.1.196",
                                       Port = Convert.ToInt32(txtPort.Text),//1883,//Convert.ToInt32(txtb_serverport.Text.Trim()),
                                       BufferSize = 20 * 400,
                                   },
                                   //ChannelOptions = new MqttClientWebSocketOptions
                                   //{
                                   //    Uri = "ws://localhost:59690/mqtt"
                                   //}
                               };
               */
                Invoke((new Action(() =>
                {
                    string pw = System.Text.Encoding.UTF8.GetString(options.Credentials.Password);
                    txtReceiveMessage.AppendText($">> passwod={pw}{Environment.NewLine}");

                })))
                ;
                await mqttClient.ConnectAsync(options);
            }
            catch (Exception ex)
            {
                Invoke((new Action(() =>
                {
                    txtReceiveMessage.AppendText($"连接到MQTT服务器失败！" + Environment.NewLine + ex.Message + Environment.NewLine);
                })));
            }
        }

        /* 
                private void MqttClient_Connected(object sender, EventArgs e)
                {
                    Invoke((new Action(() =>
                    {
                        txtReceiveMessage.AppendText("已连接到MQTT服务器！" + Environment.NewLine);
                    })));
                }

                private void MqttClient_Disconnected(object sender, EventArgs e)
                {
                    showMsg("已断开MQTT连接！");
                    Invoke((new Action(() =>
                    {
                        btnSubscribe.Enabled = true;
                        txtSubTopic.Enabled = true;
                    })));
                }

               private void MqttClient_ApplicationMessageReceived(object sender, MqttApplicationMessageReceivedEventArgs e)
                {
                    Invoke((new Action(() =>
                    {
                        txtReceiveMessage.AppendText($">> {Encoding.UTF8.GetString(e.ApplicationMessage.Payload)}{Environment.NewLine}");

                    })));
                }*/

        public Task HandleConnectedAsync(MqttClientConnectedEventArgs eventArgs)
        {
            Task t = new Task(() =>
            {
                Invoke((new Action(() =>
                {
                    txtReceiveMessage.AppendText("已连接到MQTT服务器！" + Environment.NewLine);
                })));
            });
            t.Start();
            t.ContinueWith((task) =>
            {
                Console.WriteLine("任务完成，状态为：已连接到MQTT服务器！");
            });
            return t;
        }

        public Task HandleDisconnectedAsync(MqttClientDisconnectedEventArgs eventArgs)
        {
            Task t = new Task(() =>
            {
                showMsg("已断开MQTT连接！");
                Invoke((new Action(() =>
                {
                    btnSubscribe.Enabled = true;
                    txtSubTopic.Enabled = true;
                })));
            });
            t.Start();
            t.ContinueWith((task) =>
            {
                Console.WriteLine("任务完成，状态为：已连接到MQTT服务器！");
            });
            return t;
        }

        public Task HandleApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            //throw new NotImplementedException();
            MqttApplicationMessage msg = eventArgs.ApplicationMessage;
            Task t = new Task(() =>
            {
                Invoke((new Action(() =>
                {
                    txtReceiveMessage.AppendText($">> 收到消息： ClientId={eventArgs.ClientId}. Topic={msg.Topic}{Environment.NewLine}");
                    txtReceiveMessage.AppendText($">>>> {Encoding.UTF8.GetString(msg.Payload)}{Environment.NewLine}");

                })));
            });
            t.Start();
            t.ContinueWith((task) =>
            {
                Console.WriteLine("任务完成，状态为：接收消息！");
            });
            return t;
        }



        /*        private void SubTopic(String topic)
                {
                    List<TopicFilter> list = new List<TopicFilter>();
                    string[] topics = topic.Split(new char[] { ',' });
                    foreach (string str in topics)
                    {
                        list.Add(new TopicFilter(str, MqttQualityOfServiceLevel.AtMostOnce));
                    }
                    mqttClient.SubscribeAsync(list);

                    showMsg($"已订阅[{topic}]主题");
                    txtSubTopic.Enabled = false;
                    btnSubscribe.Enabled = false;
                }*/

        private void SubscribeTopic(String topic)
        {
            List<MqttTopicFilter> list = new List<MqttTopicFilter>();
            string[] topics = topic.Split(new char[] { ',' });
            foreach (string str in topics)
            {
                MqttTopicFilter topicFilter = new MqttTopicFilter();
                topicFilter.Topic = str;
                topicFilter.QualityOfServiceLevel = MqttQualityOfServiceLevel.AtMostOnce;
                list.Add(topicFilter);
            }
            mqttClient.SubscribeAsync(list.ToArray());

            showMsg($"已订阅[{topic}]主题");
            txtSubTopic.Enabled = false;
            btnSubscribe.Enabled = false;
        }

        private void BtnSubscribe_ClickAsync(object sender, EventArgs e)
        {
            string topic = txtSubTopic.Text.Trim();

            if (string.IsNullOrEmpty(topic))
            {
                MessageBox.Show("订阅主题不能为空！");
                return;
            }

            if (mqttClient == null || !mqttClient.IsConnected)
            {
                MessageBox.Show("MQTT客户端尚未连接！");
                return;
            }

            int length = 1;
            string strCount = txtSubCount.Text.Trim();
            if (!"".Equals(strCount))
            {
                length = Convert.ToInt32(txtSubCount.Text);
            }

            for (int i = 0; i < length; i++)
            {
                SubscribeTopic(topic);
                //M2MqttUtilts.Subscribe(topic);
            }
        }



        private void BtnPublish_Click(object sender, EventArgs e)
        {
            isCancelSend = false;
            string topic = txtPubTopic.Text.Trim();

            if (string.IsNullOrEmpty(topic))
            {
                MessageBox.Show("发布主题不能为空！");
                return;
            }
            if (mqttClient == null || !mqttClient.IsConnected)
            {
                MessageBox.Show("MQTT客户端尚未连接！");
                return;
            }
            int length = 1;
            string strCount = txtSendCount.Text.Trim();
            if (!"".Equals(strCount))
            {
                length = Convert.ToInt32(txtSendCount.Text);
            }
            string inputString = txtSendMessage.Text.Trim();
            var appMsg = new MqttApplicationMessage();// (topic, Encoding.UTF8.GetBytes(inputString), MqttQualityOfServiceLevel.AtMostOnce, false);
            appMsg.Topic = topic;
            appMsg.Payload = Encoding.UTF8.GetBytes(inputString);
            appMsg.QualityOfServiceLevel = MqttQualityOfServiceLevel.AtMostOnce;
            appMsg.Retain = false;
            for (int i = 0; i < length; i++)
            {
                Application.DoEvents();
                if (isCancelSend)
                {
                    showMsg("用户取消了发送.");
                    break;
                }
                mqttClient.PublishAsync(appMsg);
                //M2MqttUtilts.Publish(appMsg.Topic, inputString);
                if ((i + 1) % 1000 == 0)
                {
                    showMsg(string.Format("发送了{0}", i + 1));
                }
            }

        }

        private void btnConnectServer_Click(object sender, EventArgs e)
        {
            Task.Run(async () => { await ConnectMqttServerAsync(); });
            //M2MqttUtilts.ConnectMqtt(txtServerIP.Text, Convert.ToInt32(txtPort.Text), txtClientId.Text, txtUsername.Text, txtPassword.Text);
        }

        private void btnCloseServer_Click(object sender, EventArgs e)
        {
            if (mqttClient == null || !mqttClient.IsConnected) { return; }
            mqttClient.DisconnectAsync();
        }

        private void btnUnSubscribe_Click(object sender, EventArgs e)
        {
            string topic = txtSubTopic.Text.Trim();

            if (string.IsNullOrEmpty(topic))
            {
                MessageBox.Show("订阅主题不能为空！");
                return;
            }
            if (!mqttClient.IsConnected)
            {
                MessageBox.Show("MQTT客户端尚未连接！");
                return;
            }

            string[] topics = topic.Split(new char[] { ',' });
            List<string> list = new List<string>();
            foreach (string str in topics)
            {
                list.Add(str);
            }
            mqttClient.UnsubscribeAsync(list.ToArray());

            showMsg($"取消订阅[{topic}]主题");
            txtSubTopic.Enabled = true;
            btnSubscribe.Enabled = true;
        }

        private void txtSendMessage_TextChanged(object sender, EventArgs e)
        {

        }

        private void showMsg(string msg)
        {
            Invoke((new Action(() =>
            {
                // if(txtReceiveMessage.Lines.Length > 2000)
                //{
                //    for (int i = 0; i < 1000; i++)
                //    {
                //        txtReceiveMessage.Lines.de
                //    } 
                //}
                int rows = 100;
                if (txtReceiveMessage.Lines.Length > rows)
                {
                    string[] sLines = txtReceiveMessage.Lines;
                    string[] sNewLines = new string[sLines.Length - rows];

                    Array.Copy(sLines, rows, sNewLines, 0, sNewLines.Length);
                    txtReceiveMessage.Lines = sNewLines;
                }
                txtReceiveMessage.AppendText(msg + Environment.NewLine);
            })));
        }

        private void btnSendCancel_Click(object sender, EventArgs e)
        {
            isCancelSend = false;
        }

        private void txtReceiveMessage_TextChanged(object sender, EventArgs e)
        {
            txtReceiveMessage.SelectionStart = txtReceiveMessage.Text.Length;
            txtReceiveMessage.ScrollToCaret();
        }

        private void btnResetClientId_Click(object sender, EventArgs e)
        {
            txtClientId.Text = Guid.NewGuid().ToString();
        }
    }
}
