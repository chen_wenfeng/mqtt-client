﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace WinMqttClient
{
    class M2MqttUtilts
    {
        private static MqttClient client = null;

        public static void ConnectMqtt(string targetServer, int port, string mqttClientId, string mqttUserName, string mqttPassword)
        {
            try
            {
                client = new MqttClient(targetServer);
                client.ProtocolVersion = MqttProtocolVersion.Version_3_1_1;

                client.Connect(mqttClientId, mqttUserName, mqttPassword, false, 60);
                client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;

            }
            catch (Exception e)
            {
                Console.WriteLine("连接失败：" + e.Message);
                throw e;
            }     
        }

        public static void Subscribe(String topic)
        {
            if(client == null)
            {
                Console.WriteLine("m2mqtt未创建");
                return;
            }

            if(!client.IsConnected)
            {
                Console.WriteLine("m2mqtt未连接");
                return;
            }

            // 从自定义Topic订阅消息
            client.Subscribe(new string[] { topic }, new byte[] { 0 });
        }

        public static void Publish(String topic, string content)
        {
            if (client == null)
            {
                Console.WriteLine("m2mqtt未创建");
                return;
            }

            if (!client.IsConnected)
            {
                Console.WriteLine("m2mqtt未连接");
                return;
            }
            var id = client.Publish(topic, Encoding.UTF8.GetBytes(content));
        }


        /// <summary>
        /// 设置订阅回调
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            // handle message received
            string topic = e.Topic;
            string message = Encoding.UTF8.GetString(e.Message);
            Console.WriteLine("Device receive message topic :" + topic + " ,the message body is " + message);
        }
    }
}
