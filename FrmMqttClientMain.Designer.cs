﻿namespace WinMqttClient
{
    partial class FrmMqttClientMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubscribe = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubTopic = new System.Windows.Forms.TextBox();
            this.txtSendMessage = new System.Windows.Forms.TextBox();
            this.txtPubTopic = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPublish = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtServerIP = new System.Windows.Forms.TextBox();
            this.btnConnectServer = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.btnCloseServer = new System.Windows.Forms.Button();
            this.btnUnSubscribe = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnResetClientId = new System.Windows.Forms.Button();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClientId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSubCount = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtReceiveMessage = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnSendCancel = new System.Windows.Forms.Button();
            this.txtSendCount = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.txtCommunicationTimeout = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtKeepAlivePeriod = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkCleanSession = new System.Windows.Forms.CheckBox();
            this.cbbMqttProtocolVersion = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSubscribe
            // 
            this.btnSubscribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubscribe.Location = new System.Drawing.Point(522, 81);
            this.btnSubscribe.Name = "btnSubscribe";
            this.btnSubscribe.Size = new System.Drawing.Size(75, 23);
            this.btnSubscribe.TabIndex = 0;
            this.btnSubscribe.Text = "订阅";
            this.btnSubscribe.UseVisualStyleBackColor = true;
            this.btnSubscribe.Click += new System.EventHandler(this.BtnSubscribe_ClickAsync);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "订阅主题";
            // 
            // txtSubTopic
            // 
            this.txtSubTopic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubTopic.Location = new System.Drawing.Point(79, 82);
            this.txtSubTopic.Name = "txtSubTopic";
            this.txtSubTopic.Size = new System.Drawing.Size(354, 21);
            this.txtSubTopic.TabIndex = 2;
            this.txtSubTopic.Text = "/sys/a19C24F7BFV/printer001/thing/event/property/post";
            // 
            // txtSendMessage
            // 
            this.txtSendMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSendMessage.Location = new System.Drawing.Point(0, 51);
            this.txtSendMessage.Multiline = true;
            this.txtSendMessage.Name = "txtSendMessage";
            this.txtSendMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSendMessage.Size = new System.Drawing.Size(708, 232);
            this.txtSendMessage.TabIndex = 7;
            this.txtSendMessage.TextChanged += new System.EventHandler(this.txtSendMessage_TextChanged);
            // 
            // txtPubTopic
            // 
            this.txtPubTopic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPubTopic.Location = new System.Drawing.Point(79, 17);
            this.txtPubTopic.Name = "txtPubTopic";
            this.txtPubTopic.Size = new System.Drawing.Size(354, 21);
            this.txtPubTopic.TabIndex = 6;
            this.txtPubTopic.Text = "/sys/a19C24F7BFV/printer001/thing/event/property/post";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "发布主题";
            // 
            // btnPublish
            // 
            this.btnPublish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPublish.Location = new System.Drawing.Point(522, 16);
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(75, 23);
            this.btnPublish.TabIndex = 4;
            this.btnPublish.Text = "发送";
            this.btnPublish.UseVisualStyleBackColor = true;
            this.btnPublish.Click += new System.EventHandler(this.BtnPublish_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "服端器IP";
            // 
            // txtServerIP
            // 
            this.txtServerIP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerIP.Location = new System.Drawing.Point(79, 7);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.Size = new System.Drawing.Size(274, 21);
            this.txtServerIP.TabIndex = 9;
            this.txtServerIP.Text = "a19C24F7BFV.iot-as-mqtt.cn-shanghai.aliyuncs.com";
            // 
            // btnConnectServer
            // 
            this.btnConnectServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnectServer.Location = new System.Drawing.Point(522, 6);
            this.btnConnectServer.Name = "btnConnectServer";
            this.btnConnectServer.Size = new System.Drawing.Size(75, 23);
            this.btnConnectServer.TabIndex = 10;
            this.btnConnectServer.Text = "连接服务";
            this.btnConnectServer.UseVisualStyleBackColor = true;
            this.btnConnectServer.Click += new System.EventHandler(this.btnConnectServer_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(370, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "端口";
            // 
            // txtPort
            // 
            this.txtPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPort.Location = new System.Drawing.Point(405, 7);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(99, 21);
            this.txtPort.TabIndex = 12;
            this.txtPort.Text = "1883";
            // 
            // btnCloseServer
            // 
            this.btnCloseServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseServer.Location = new System.Drawing.Point(612, 6);
            this.btnCloseServer.Name = "btnCloseServer";
            this.btnCloseServer.Size = new System.Drawing.Size(75, 23);
            this.btnCloseServer.TabIndex = 13;
            this.btnCloseServer.Text = "断开";
            this.btnCloseServer.UseVisualStyleBackColor = true;
            this.btnCloseServer.Click += new System.EventHandler(this.btnCloseServer_Click);
            // 
            // btnUnSubscribe
            // 
            this.btnUnSubscribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUnSubscribe.Location = new System.Drawing.Point(612, 81);
            this.btnUnSubscribe.Name = "btnUnSubscribe";
            this.btnUnSubscribe.Size = new System.Drawing.Size(75, 23);
            this.btnUnSubscribe.TabIndex = 14;
            this.btnUnSubscribe.Text = "取消订阅";
            this.btnUnSubscribe.UseVisualStyleBackColor = true;
            this.btnUnSubscribe.Click += new System.EventHandler(this.btnUnSubscribe_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbbMqttProtocolVersion);
            this.panel1.Controls.Add(this.chkCleanSession);
            this.panel1.Controls.Add(this.txtKeepAlivePeriod);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtCommunicationTimeout);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.btnResetClientId);
            this.panel1.Controls.Add(this.txtUsername);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtClientId);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtSubCount);
            this.panel1.Controls.Add(this.txtPort);
            this.panel1.Controls.Add(this.btnUnSubscribe);
            this.panel1.Controls.Add(this.btnSubscribe);
            this.panel1.Controls.Add(this.btnCloseServer);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtSubTopic);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnConnectServer);
            this.panel1.Controls.Add(this.txtServerIP);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 140);
            this.panel1.TabIndex = 15;
            // 
            // btnResetClientId
            // 
            this.btnResetClientId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetClientId.Location = new System.Drawing.Point(372, 31);
            this.btnResetClientId.Name = "btnResetClientId";
            this.btnResetClientId.Size = new System.Drawing.Size(75, 23);
            this.btnResetClientId.TabIndex = 23;
            this.btnResetClientId.Text = "生成ClientId";
            this.btnResetClientId.UseVisualStyleBackColor = true;
            this.btnResetClientId.Click += new System.EventHandler(this.btnResetClientId_Click);
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsername.Location = new System.Drawing.Point(79, 57);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(274, 21);
            this.txtUsername.TabIndex = 22;
            this.txtUsername.Text = "printer001&a19C24F7BFV";
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(407, 57);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(280, 21);
            this.txtPassword.TabIndex = 21;
            this.txtPassword.Text = "6862230543E25A6DC732A4815D55F6DD49D57D19";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(370, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "密码";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 18;
            this.label6.Text = "用户";
            // 
            // txtClientId
            // 
            this.txtClientId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtClientId.Location = new System.Drawing.Point(79, 31);
            this.txtClientId.Name = "txtClientId";
            this.txtClientId.Size = new System.Drawing.Size(274, 21);
            this.txtClientId.TabIndex = 17;
            this.txtClientId.Text = "12345|securemode=3,signmethod=hmacsha1|";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "ClientId";
            // 
            // txtSubCount
            // 
            this.txtSubCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubCount.Location = new System.Drawing.Point(439, 81);
            this.txtSubCount.Name = "txtSubCount";
            this.txtSubCount.Size = new System.Drawing.Size(67, 21);
            this.txtSubCount.TabIndex = 15;
            this.txtSubCount.Text = "1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtReceiveMessage);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(708, 218);
            this.panel2.TabIndex = 16;
            // 
            // txtReceiveMessage
            // 
            this.txtReceiveMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReceiveMessage.Location = new System.Drawing.Point(0, 140);
            this.txtReceiveMessage.Name = "txtReceiveMessage";
            this.txtReceiveMessage.Size = new System.Drawing.Size(708, 78);
            this.txtReceiveMessage.TabIndex = 16;
            this.txtReceiveMessage.Text = "";
            this.txtReceiveMessage.TextChanged += new System.EventHandler(this.txtReceiveMessage_TextChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtSendMessage);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 218);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(708, 283);
            this.panel3.TabIndex = 17;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnSendCancel);
            this.panel4.Controls.Add(this.txtSendCount);
            this.panel4.Controls.Add(this.txtPubTopic);
            this.panel4.Controls.Add(this.btnPublish);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(708, 51);
            this.panel4.TabIndex = 18;
            // 
            // btnSendCancel
            // 
            this.btnSendCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendCancel.Location = new System.Drawing.Point(612, 16);
            this.btnSendCancel.Name = "btnSendCancel";
            this.btnSendCancel.Size = new System.Drawing.Size(75, 23);
            this.btnSendCancel.TabIndex = 8;
            this.btnSendCancel.Text = "取消";
            this.btnSendCancel.UseVisualStyleBackColor = true;
            this.btnSendCancel.Click += new System.EventHandler(this.btnSendCancel_Click);
            // 
            // txtSendCount
            // 
            this.txtSendCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSendCount.Location = new System.Drawing.Point(439, 17);
            this.txtSendCount.Name = "txtSendCount";
            this.txtSendCount.Size = new System.Drawing.Size(67, 21);
            this.txtSendCount.TabIndex = 7;
            this.txtSendCount.Text = "1";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 218);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(708, 3);
            this.splitter1.TabIndex = 18;
            this.splitter1.TabStop = false;
            // 
            // txtCommunicationTimeout
            // 
            this.txtCommunicationTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommunicationTimeout.Location = new System.Drawing.Point(138, 109);
            this.txtCommunicationTimeout.Name = "txtCommunicationTimeout";
            this.txtCommunicationTimeout.Size = new System.Drawing.Size(50, 21);
            this.txtCommunicationTimeout.TabIndex = 25;
            this.txtCommunicationTimeout.Text = "10";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "Connect Timeout(s)";
            // 
            // txtKeepAlivePeriod
            // 
            this.txtKeepAlivePeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeepAlivePeriod.Location = new System.Drawing.Point(283, 109);
            this.txtKeepAlivePeriod.Name = "txtKeepAlivePeriod";
            this.txtKeepAlivePeriod.Size = new System.Drawing.Size(50, 21);
            this.txtKeepAlivePeriod.TabIndex = 27;
            this.txtKeepAlivePeriod.Text = "60";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(194, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 12);
            this.label9.TabIndex = 26;
            this.label9.Text = "Keep Alive(s)";
            // 
            // chkCleanSession
            // 
            this.chkCleanSession.AutoSize = true;
            this.chkCleanSession.Location = new System.Drawing.Point(345, 111);
            this.chkCleanSession.Name = "chkCleanSession";
            this.chkCleanSession.Size = new System.Drawing.Size(102, 16);
            this.chkCleanSession.TabIndex = 28;
            this.chkCleanSession.Text = "Clean Session";
            this.chkCleanSession.UseVisualStyleBackColor = true;
            // 
            // cbbMqttProtocolVersion
            // 
            this.cbbMqttProtocolVersion.FormattingEnabled = true;
            this.cbbMqttProtocolVersion.Items.AddRange(new object[] {
            "3.1.0",
            "3.1.1",
            "5.0.0"});
            this.cbbMqttProtocolVersion.Location = new System.Drawing.Point(453, 108);
            this.cbbMqttProtocolVersion.Name = "cbbMqttProtocolVersion";
            this.cbbMqttProtocolVersion.Size = new System.Drawing.Size(121, 20);
            this.cbbMqttProtocolVersion.TabIndex = 29;
            this.cbbMqttProtocolVersion.Text = "3.1.1";
            // 
            // FrmMqttClientMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 501);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "FrmMqttClientMain";
            this.Text = "MQTT-Client";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSubscribe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSubTopic;
        private System.Windows.Forms.TextBox txtSendMessage;
        private System.Windows.Forms.TextBox txtPubTopic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPublish;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtServerIP;
        private System.Windows.Forms.Button btnConnectServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Button btnCloseServer;
        private System.Windows.Forms.Button btnUnSubscribe;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox txtSendCount;
        private System.Windows.Forms.Button btnSendCancel;
        private System.Windows.Forms.RichTextBox txtReceiveMessage;
        private System.Windows.Forms.TextBox txtSubCount;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClientId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnResetClientId;
        private System.Windows.Forms.TextBox txtKeepAlivePeriod;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCommunicationTimeout;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkCleanSession;
        private System.Windows.Forms.ComboBox cbbMqttProtocolVersion;
    }
}

